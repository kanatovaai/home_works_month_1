# Напишите пример функции возвращающей значения с помощью return
def sum():
    result = 'go to the university'
    return result
print(sum())



# Напишите пример функции возвращающей значения с помощью return и принимающей в себя любое колличество аргументов
def sum(a, b, c, d):
    result = a + b * (c - d)
    return result
print(sum(123, 34, 44, 21))

def text(a, b, c, d, e):
    result = a + b + c + d + e
    return result
print(text('Hello', ' everyone!', ' I', ' am', ' Aidai.'))

# Напишите пример цикла в цикле

friends = [['Aidana', 'Sara', 'Amina', 'Dinara'], ['Islam', 'Vladislav', 'Sultan', 'Bekbol']]

for array_friends in friends:
    for friend in array_friends:
        print(friend)


# Напишите пример клуба с двумя функциями (функция охранник, функция администратор), функция охранника должна принимать в себя любое количество массивов с людьми

people = [
    {
        'name': 'Kadyrbek',
        'age': 25,
        'hair colour': 'blond',
        'gender': 'male'
    },
    {
        'name': 'Aizada',
        'age': 17,
        'hair colour': 'brunette',
        'gender': 'male'
    },
    {
        'name': 'Nurbek',
        'age': 22,
        'hair colour': 'brunette',
        'gender': 'female'
    },
    {
        'name': 'Mirana',
        'age': 20,
        'hair colour': 'blond',
        'gender': 'female'
    },
    {
        'name': 'Bogdan',
        'age': 16,
        'hair colour': 'brunette',
        'gender': 'male'
    },
    {
        'name': 'Aizada',
        'age': 26,
        'hair colour': 'brunette',
        'gender': 'male'
    }
]

for person in people:
    print(person)


def ohrannik(clients):
    verified_people = []
    for client in clients:
        if client['age'] >= 18:
            verified_people.append(client)
    return verified_people


def administrator(clients):
    for client in clients:
        client['status'] = True



print('---------------------')

go_to_the_club = ohrannik(people)
administrator(go_to_the_club)

for person in people:
    print(person)
