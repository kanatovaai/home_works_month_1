# Вставляйте решение под задачей

# Пример:
# Создайте переменную с именем carname и присвойте ей значение Volvo.
carname = 'volvo'


# Выведите переменную функцией print
print(carname)

# Создайте несколько строковых переменных
variable_1 = 'Asel,'
variable_2 = 'Aidai,'
variable_3 = 'Islam,'
variable_4 = 'Bektur.'

# Сложите строковые переменные
result = variable_1 + variable_2 + variable_3 + variable_4
print(result)

# Создайте несколько числовых переменных
fruit = 6
vegetables = 55
meat_products = 120
dairy_products = 48

# Сложите числовые переменные
result = fruit + vegetables + meat_products + dairy_products
print(result)

# Вычтите одну числовую переменную из другой
result = vegetables - fruit
print(result)

# Выведите произведение одной переменной на другую
result = meat_products * dairy_products
print(result)

# Выведите деление одной переменной на другую
result = meat_products / fruit
print(result)

# Попробуйте использовать дробные числа (2.5)
a = 4.5
b = 8.6
c = a - b
d = a + b
e = a * b
print(c, d, e)




