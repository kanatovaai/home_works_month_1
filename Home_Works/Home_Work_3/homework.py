# Напишите пример сравнения c использованием оператора (>)
a = 887
b = 89
print(a > b)

# Напишите пример сравнения c использованием оператора (<)
f = 384
h = 665
print(f < h)

# Напишите пример сравнения c использованием оператора (==)
c = 445
d = 446
print(c == d)

# Напишите пример сравнения c использованием оператора (>=)
Bananas = (34 + 2) * 1
Apples = (2 * 2) + 4
print(Bananas >= Apples)

# Напишите пример сравнения c использованием оператора (<=)
Books = 56 * 2
Copybooks = (48 * 14) + (12 * 36)
print(Books <= Copybooks)

# Напишите пример сравнения c использованием оператора (<) несколько раз
S = (2 ** 3) - 34
R = 45 + (3 ** 4)
print(S < R)


English_courses = 200 * 12
French_courses = 211 * 16
print(English_courses < French_courses)

first_example = 23 + (3 ** 2 - (56 / 7)) * 1
second_example = (2 ** 4) + (45 + 67 * (81 / 9) + 2)
print(first_example < second_example)

# Напишите пример сравнения c использованием оператора (>) несколько раз
Bishkek_Osh = 608
Bishkek_Karakol = 403
print(Bishkek_Osh > Bishkek_Karakol)

population_in_Germany = 83020000
population_in_USA = 328200000
print(population_in_Germany > population_in_USA)

Have_covid19_in_Brazil = 1209910
Have_covid19_in_Mexico = 256165
print(Have_covid19_in_Brazil > Have_covid19_in_Mexico)


# Напишите пример использования конструкции if
podruga = {
    'name': 'Adelya',
    'surname': 'Kojomuratova',
    'age': 17
}
if podruga['name'] == 'Adelya':
    print('--- da ---')


# Напишите пример использования конструкции if / else
podruga = {
    'name': 'Asyl',
    'surname': 'Umetalieva',
    'age': 22
}
if podruga['name'] == 'Adelya':
    print('--- podruga ---')
else:
    print('--- ne podruga ----')

# Напишите пример использования конструкции if / elif / else
team_1 = 204
team_2 = 178

if team_2 >= 190:
    print('---- Won ----')
elif team_2 < 190:
    print('---- lost ----')
else:
    print('---- lost ----')


# Создайте список (list) и поместите в него все изученные типы данных
l = list(['bool', 'strings', 'numbers', 'dict', 'list'])
print(l)

# Удалите из ранее созданного списка какой либо объект
l.pop(2)
print(l)
# Замените какой либо объект из ранее созданного списка
l[1] = 'text'
print(l)
# Создайте словарь (dict) и поместите в него все изученные типы данных
d = {
    'bool': 'true',
    'strings': 'hello',
    'numbers': 13,
    'list': [1, 2, 3]
}

print(d)
# Удалите из ранее созданного списка какой либо объект
del l[0]
print(l)


# Замените какой либо объект из ранее созданного списка

l[0] = 'Hello'
l[2] = [1, 2, 3, 4]
print(l)


print(d.items())
# Напишите пример использования метода списка append()
l.append('true')
print(l)

# Напишите пример использования метода списка clear() (нужно прочитать самостоятельно)
d.clear()
print(d)
# Напишите пример использования метода списка reverse() (нужно прочитать самостоятельно
l.reverse()
print(l)


# Напишите пример использования метода словаря get() (нужно прочитать самостоятельно)
d = {
    'bool': 'true',
    'strings': 'hello',
    'numbers': 13,
    'list': [1, 2, 3]
}

print(d)

print(d.get('strings'))

# Напишите пример использования метода строки find() (нужно прочитать самостоятельно
text = 'She has a cat'
print(text.find('h'))

# Напишите пример использования метода строки split() (нужно прочитать самостоятельно)
text = 'I have dog'
print(text.split())
