# Напишите пример держащий функцию принимающую *args

def f(*args):
    print('----- args -----', args)

f(1, 2, 3, 4, 5, 6, 7, 8)


def my_sum(*args):
    s = 0
    for i in args:
        s += i
    return s

print(my_sum(1, 2, 3, 4, 5, 6, 7, 8))


# Напишите пример содержащий функцию принимающую *kwargs

def create_document(**kwargs):
    print('----- kwargs -----', kwargs)


create_document(a=1, b=2, c=3, d=4)


# Напишите пример содержащий функцию принимающую *args и *kwargs

def my_document(*args, **kwargs):
    print('-- args --', args)
    print('-- kwargs --', kwargs)

my_document(2, 3, 4, 35, 5, 67, 83, a=1, b=2, c=3, d=4)
# Напишите пример содержащий функцию принимающую *args и *kwargs а также простые агрументы
def document(a, b, *args, **kwargs):
    print('---a---', a)
    print('---b---', b)
    print('---args---', args)
    print('---kwargs---', kwargs)

document(1, 2, 3, 4, 5, j=5, f=6, g=76)
# Напишите пример lambda функции которая возвращает результат умножения числа пришедшего в аргументы на 5
x = lambda b: b * 5
print(x(9))

# Напишите пример lambda функции которая возвращает результат деления числа пришедшего в аргументы на 5
f = lambda i: i / 5
print(f(50))

k = lambda l: l ** 2
print(k(7))
# Напишите пример lambda функции которая возвращает значение 'age' из пришедшего в аргументы словаря
human = dict(
    {
        'name': 'Asylebek',
        'age': 27,
        'gender': 'male'
    }
)



# Напишите пример создания list
a = [5, 16, 'apple', 'hello', [9, 6, 57]]

# Напишите чтения из list
my_list = ['Aidai', 'pen', 356, 78, 'water', ['book', 'pen']]
print(my_list)
# Напишите пример изменения list
my_list[0] = 'kanatova'
print(my_list)
# Напишите пример добавления в list
my_list.append(98)
print(my_list)
# Напишите пример удаления из list
del my_list[1]
print(my_list)

# Напишите пример перевода list в tuple
print(tuple(my_list))
# Напишите пример создания tuple
my_tuple = ('banana', 'apple', 'cherry', 'strawberry')

# Напишите чтения из tuple
my_tuple = ('banana', 'apple', 'cherry', 'strawberry')
print(my_tuple)
# Напишите пример перевода tuple в list
print(list(my_tuple))

# Напишите пример создания dict
d = {'Lena': 1, 'Andrei': 2, 'Sasha': 3, 'Eren': 4, 'Mikasa': 5}
# Напишите чтения из dict
d = {'Lena': 1, 'Andrei': 2, 'Sasha': 3, 'Eren': 4, 'Mikasa': 5}
print(d)
# Напишите пример изменения dict
d['Mikasa'] = 9
print(d)
# Напишите пример добавления в dict
d['Aizada'] = 6
print(d)
# Напишите пример удаления из dict
print(d.pop('Sasha'))
print(d)
# Напишите пример применения lambda функции в методе list (sort)

# Почитате про python sorted

# Напишите пример применения lambda функции в методе list (sorted)

# Напишите пример создания set
my_set = {9, 2, 3, 2, 4, 5, 6, 'hello'}
print(my_set)

my_second_set = {16, 21, 123, 568, 52, 'phone', 'flowers'}
print(my_second_set)

print('k, k')
