# Создайте пример использования циклов с массивом данных состоящих из цифр
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for members in my_list:
    print(members)

# Создайте пример использования циклов с массивом данных состоящих из словарей (dict)
room = [['pen, copybook'], ['laptop, laptop charger'], ['table, chair']]
for couple in room:
    print(couple)


# Создайте простую функцию - выводящую в  консоль слово 'Horosho'
def word():
    print('Horosho')

word()


# Создайте простую функцию - складывающую 5 + 5
def sum():
    print(5 + 5)


sum()


# Создайте простую функцию - которая принимает в себя число и отнимает от принятого числа 5
def exemple():
    print(165 - 5)


exemple()


# Создайте простую функцию - которая принимает в себя число и умножает его на 5
def exemple_2():
    print(674 * 5)


exemple_2()


# Создайте простую функцию - которая принимает в себя число и вычисляет 15 процентов от принятого числа
def exemple_3():
    print(1675 % 15)


exemple_3()


# Создайте простую функцию - которая принимает в себя строку и добавляет ей в конец слово 'hello'
def exemple_4():
    print('Aidai Kanatova' + ' hello')


exemple_4()

# Создать свой клуб с использованием ммассива людей, и функции охранника для фильтрации людей
my_friends = [
    {
        'name': 'Adelya',
        'age': '18',
        'hair colour': 'brunette',
        'height': 173,
        'gender': 'female'
    },
    {
        'name': 'Zarina',
        'age': '19',
        'hair colour': 'blond',
        'height': 165,
        'gender': 'female'
    },
    {
        'name': 'Aliya',
        'age': '16',
        'hair colour': 'brunette',
        'height': 166,
        'gender': 'female'
    },
    {
        'name': 'Ilona',
        'age': '20',
        'hair colour': 'blond',
        'height': 170,
        'gender': 'female'
    },
    {
        'name': 'Asyla',
        'age': '15',
        'hair colour': 'brunette',
        'height': 169,
        'gender': 'female'
    }
]

def ohrannik(clients):
    for client in clients:
        if client['age'] >= '18':
            print('---- go to the club ----', client['name'])
        else:
            print('---- do not go to the club ----', client['name'])


ohrannik(my_friends)
